package service

type ServiceInfo struct {
	Name          string
	IPAddresses   []string
	Subscriptions []string
	NumWorkers    int64
	MaxJobs       int64
}
