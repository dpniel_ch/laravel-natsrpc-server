package service

import (
	"encoding/json"
	"fmt"
	"net"
	"net/rpc"
	"os"
	"time"

	nats "github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"github.com/spiral/goridge/v2"
)

type ClientConfig struct {
	Name string
	Host string
	User string
	Pass string
}

type Payload struct {
	Service string                 `json:"service"`
	Data    map[string]interface{} `json:"data"`
	Timeout int                    `json:"timeout,omitempty"`
}

func (p *Payload) TimeoutDuration() time.Duration {
	if p.Timeout > 0 {
		return time.Duration(p.Timeout) * time.Second
	}
	return 2 * time.Second
}

type Client struct {
	nats *nats.EncodedConn
}

func (c *Client) Publish(msg string, resp *string) error {
	var payload Payload
	err := json.Unmarshal([]byte(msg), &payload)
	if err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"type":    "PUB",
		"subject": payload.Service,
	}).Info(msg)

	return c.nats.Publish(payload.Service, &payload.Data)
}

func (c *Client) Request(msg string, resp *string) error {
	var payload Payload
	err := json.Unmarshal([]byte(msg), &payload)
	if err != nil {
		return err
	}
	start := time.Now()
	var reply string
	err = c.nats.Request(payload.Service, &payload.Data, &reply, payload.TimeoutDuration())
	if err != nil {
		return err
	}
	*resp = reply

	log.WithFields(log.Fields{
		"type":     "REQ",
		"req_time": time.Since(start),
		"subject":  payload.Service,
	}).Info(msg)

	return nil
}

func newClient(nc *nats.Conn) *Client {
	ec, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	return &Client{
		nats: ec,
	}
}

type ClientRPC struct {
	cfg  *ClientConfig
	nats *nats.Conn
}

func NewRpcClient(cfg *ClientConfig) *ClientRPC {
	return &ClientRPC{
		cfg:  cfg,
		nats: nil,
	}
}

func (crpc *ClientRPC) Listen() {
	sock := fmt.Sprintf("/tmp/%s.sock", crpc.cfg.Name)
	if crpc.cfg.User != "" {
		sock = fmt.Sprintf("/tmp/%s.%s.sock", crpc.cfg.Name, crpc.cfg.User)
	}
	if err := os.RemoveAll(sock); err != nil {
		log.Fatal(err)
	}
	// ln, err := net.Listen("tcp", ":6001")
	ln, err := net.Listen("unix", sock)
	if err != nil {
		panic(err)
	}

	nc, err := nats.Connect(crpc.cfg.Host, nats.UserInfo(crpc.cfg.User, crpc.cfg.Pass))
	if err != nil {
		panic(err)
	}
	crpc.nats = nc

	rpc.Register(newClient(nc))

	for {
		conn, err := ln.Accept()
		if err != nil {
			continue
		}
		go rpc.ServeCodec(goridge.NewCodec(conn))
	}
}
