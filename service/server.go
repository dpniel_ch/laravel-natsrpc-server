package service

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	nats "github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"github.com/spiral/roadrunner"
)

type Server struct {
	cfg  *ServerConfig
	srv  *roadrunner.Server
	nats *nats.Conn
	info *ServiceInfo
}

type ServerConfig struct {
	Name       string
	Command    string
	NumWorkers int64
	MaxJobs    int64
	Subscribe  string
	Host       string
	User       string
	Pass       string
	Admin      bool
}

// NewServer returns new natsrpc server
func NewServer(cfg *ServerConfig) *Server {
	s := &Server{
		cfg:  cfg,
		srv:  nil,
		nats: nil,
		info: &ServiceInfo{
			Name:          cfg.Name,
			IPAddresses:   GetIPAddresses(),
			Subscriptions: []string{},
			MaxJobs:       cfg.MaxJobs,
			NumWorkers:    cfg.NumWorkers,
		},
	}
	fmt.Println(GetIPAddresses())
	s.srv = roadrunner.NewServer(&roadrunner.ServerConfig{
		Command: cfg.Command,
		Relay:   "pipes",
		Pool: &roadrunner.Config{
			NumWorkers:      cfg.NumWorkers,
			MaxJobs:         cfg.MaxJobs,
			AllocateTimeout: 5 * time.Second,
			DestroyTimeout:  1 * time.Second,
		},
	})

	nc, err := nats.Connect(cfg.Host, nats.Name(cfg.Name), nats.UserInfo(cfg.User, cfg.Pass))
	if err != nil {
		panic(err)
	}
	s.nats = nc
	return s
}

func (s *Server) Start() {
	s.srv.Start()

	log.WithField("service", s.cfg.Name).Info("Setting up service listener")

	// RPC Handling
	s.nats.QueueSubscribe(s.cfg.Name, strings.ReplaceAll(s.cfg.Name, ".", "_"), s.timedFuncHandler("RPC", true))

	// Service Event Subscription handling
	for _, sub := range strings.Split(s.cfg.Subscribe, ",") {
		if sub == "" {
			continue
		}
		eventSub := fmt.Sprintf("%s.events", sub)
		subGroup := fmt.Sprintf("%s_%s", s.cfg.Name, sub)

		log.WithField("service", eventSub).Info("Subscribing to service events")
		s.info.Subscriptions = append(s.info.Subscriptions, eventSub)
		s.nats.QueueSubscribe(eventSub, subGroup, s.timedFuncHandler("EVENT", false))
	}

	// Process management
	s.nats.Subscribe(fmt.Sprintf("%s.start", s.cfg.Name), s.startHandler())
	s.nats.Subscribe(fmt.Sprintf("%s.stop", s.cfg.Name), s.stopHandler())
	s.nats.Subscribe(fmt.Sprintf("%s.reset", s.cfg.Name), s.resetHandler())
	s.nats.Subscribe(fmt.Sprintf("%s.kill", s.cfg.Name), s.killHandler())

	// Service discovery
	s.nats.Subscribe("admin.service.list", s.discoveryHandler())

	if s.cfg.Admin {
		s.nats.QueueSubscribe("admin.service.info", "admin_info_queue", s.timedFuncHandler("ADMIN_EVENT", false))

		// Because admin is enabled we want to discover services every 60 seconds
		go func() {
			ticker := time.NewTicker(60 * time.Second)
			for {
				select {
				case <-ticker.C:
					log.Info("Discovering services....")
					s.nats.Publish("admin.service.list", []byte{})
				}
			}
		}()
	}
}

func (s *Server) Stop() {
	s.srv.Stop()
	s.nats.Close()
}

func (s *Server) timedFuncHandler(eventType string, respond bool) nats.MsgHandler {
	return func(m *nats.Msg) {
		start := time.Now()
		log.WithFields(log.Fields{
			"type":      eventType,
			"nats_type": "queue_sub",
			"subject":   m.Subject,
			"direction": "[IN]",
		}).Info(string(m.Data))
		res, err := s.srv.Exec(&roadrunner.Payload{Body: m.Data})
		if err != nil {
			log.Error(err)
			return
		}
		if respond {
			m.Respond([]byte(res.String()))
			log.WithFields(log.Fields{
				"type":      eventType,
				"nats_type": "queue_sub",
				"subject":   m.Subject,
				"direction": "[OUT]",
				"exec_time": time.Since(start),
			}).Info(res.String())
		}
	}
}

func (s *Server) resetHandler() nats.MsgHandler {
	return func(m *nats.Msg) {
		log.WithFields(log.Fields{
			"type":      "RESET",
			"subject":   m.Subject,
			"direction": "[IN]",
		}).Info("Resetting server")
		s.srv.Reset()
	}
}

func (s *Server) stopHandler() nats.MsgHandler {
	return func(m *nats.Msg) {
		log.WithFields(log.Fields{
			"type":      "STOP",
			"subject":   m.Subject,
			"direction": "[IN]",
		}).Info("Stopping server")
		s.srv.Stop()
	}
}

func (s *Server) startHandler() nats.MsgHandler {
	return func(m *nats.Msg) {
		log.WithFields(log.Fields{
			"type":      "START",
			"subject":   m.Subject,
			"direction": "[IN]",
		}).Info("Starting server")
		s.srv.Start()
	}
}

func (s *Server) killHandler() nats.MsgHandler {
	return func(m *nats.Msg) {
		log.WithFields(log.Fields{
			"type":      "KILL",
			"subject":   m.Subject,
			"direction": "[IN]",
		}).Info("Killing process")
		os.Exit(1)
	}
}

func (s *Server) discoveryHandler() nats.MsgHandler {
	return func(m *nats.Msg) {
		log.WithFields(log.Fields{
			"type":      "SERVICE_DISCOVERY",
			"subject":   m.Subject,
			"direction": "[IN]",
		}).Info("Discovery requested")
		info, err := json.Marshal(&s.info)
		if err != nil {
			log.Error(err)
			return
		}
		s.nats.Publish("admin.service.info", info)
		log.WithFields(log.Fields{
			"type":      "SERVICE_DISCOVERY",
			"subject":   m.Subject,
			"direction": "[OUT]",
		}).Info(string(info))
	}
}
