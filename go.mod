module bitbucket.org/dpniel_ch/laravel-natsrpc-server

go 1.15

require (
	github.com/nats-io/nats-server/v2 v2.1.8 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	github.com/spiral/goridge/v2 v2.4.5
	github.com/spiral/roadrunner v1.8.3
)

