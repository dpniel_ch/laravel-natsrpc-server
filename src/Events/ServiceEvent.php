<?php

namespace NatsRPC\Events;

use Illuminate\Queue\SerializesModels;
use NatsRPC\Server\RequestParams;

Class ServiceEvent
{
    use SerializesModels;

    /**
     * @var string
     */
    public $service;
    /**
     * @var string
     */
    public $event;

    /**
     * @var RequestParams
     */
    public $params;

    public function __construct(string $service, string $event, RequestParams $params)
    {
        $this->event = $event;
        $this->params = $params;
        $this->service = $service;
    }
}
