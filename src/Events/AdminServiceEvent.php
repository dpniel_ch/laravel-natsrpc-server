<?php

namespace NatsRPC\Events;

use Illuminate\Queue\SerializesModels;
use NatsRPC\Server\RequestParams;

Class AdminServiceEvent
{
    use SerializesModels;

    /**
     * @var string
     */
    public $service;
    /**
     * @var string
     */
    public $event;

    /**
     * @var array
     */
    public $params;

    public function __construct(string $service, string $event, array $params)
    {
        $this->event = $event;
        $this->params = $params;
        $this->service = $service;
    }
}
