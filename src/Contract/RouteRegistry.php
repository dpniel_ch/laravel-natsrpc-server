<?php

namespace NatsRPC\Contract;

interface RouteRegistry
{
    public function addMiddleware(string $middleware) : RouteRegistry;

    public function addMiddlewares(array $middlewares) : RouteRegistry;

    public function bind(string $method, string $binding) : RouteRegistry;

    public function bindController(string $namespace, string $controller) : RouteRegistry;

    public function group($middlewaresConfigurator, callable $routesConfigurator) : RouteRegistry;

    public function resolve(string $method) : Route;
}
