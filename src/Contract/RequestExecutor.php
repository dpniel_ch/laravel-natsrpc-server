<?php

namespace NatsRPC\Contract;

interface RequestExecutor
{
    public function execute(Request $request);
}
