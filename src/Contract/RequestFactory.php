<?php


namespace NatsRPC\Contract;


interface RequestFactory
{
    public function createFromPayload(string $payloadJson) : Executable;

    public function createRequest(\stdClass $requestData) : Request;
}
