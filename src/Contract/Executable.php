<?php


namespace NatsRPC\Contract;


interface Executable
{
    public function executeWith(RequestExecutor $executor);
}
