<?php


namespace NatsRPC\Contract;

use NatsRPC\Server\RequestParams;

interface EventHandler
{
    public function handle(RequestParams $params);
}
