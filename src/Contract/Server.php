<?php

namespace NatsRPC\Contract;

use Illuminate\Http\JsonResponse;

interface Server
{
    public function router() : RouteRegistry;

    public function onException(string $exceptionClass, callable $handler, bool $first = false) : Server;

    public function setPayload(string $payload) : void;

    public function setControllerNamespace(string $namespace = null) : Server;

    public function run($context = null, string $payload = null) : JsonResponse;

    public function runWithPayload(string $payload, $context = null) : JsonResponse;
}
