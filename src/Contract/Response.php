<?php

namespace NatsRPC\Contract;

interface Response
{
    public function id();

    public function hasError();

    public function hasResult();

    public function error();

    public function result();
}
