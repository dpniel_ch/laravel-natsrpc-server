<?php

namespace NatsRPC\Contract;

interface MiddlewaresConfiguration
{
    public function getMiddlewares() : array;

    public function isEmpty();

    public function setMiddlewares(array $middlewares = []) : MiddlewaresConfiguration;

    public function addMiddleware(string $middleware) : MiddlewaresConfiguration;
}
