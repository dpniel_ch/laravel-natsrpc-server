<?php

namespace NatsRPC\Contract;

use NatsRPC\Server\RequestParams;

interface Request
{
    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @return RequestParams
     */
    public function getParams(): RequestParams;

    /**
     * @return null|string|int
     */
    public function getId();

    /**
     * @return bool
     */
    public function hasId(): bool;
}
