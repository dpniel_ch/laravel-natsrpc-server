<?php

namespace NatsRPC\Contract;

interface Route
{
    public function getControllerClass() : string;

    public function getActionName() : string;

    public function getMiddlewaresCollection() : MiddlewaresConfiguration;
}
