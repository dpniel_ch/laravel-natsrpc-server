<?php


namespace NatsRPC\Contract;


interface MiddlewareDispatcher
{
    public function dispatch(MiddlewaresConfiguration $config, $context, callable $next);
}
