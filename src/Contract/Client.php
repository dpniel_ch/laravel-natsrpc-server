<?php

namespace NatsRPC\Contract;

interface Client {

    public function raw(string $topic, array $payload);

    public function publish(string $event, array $payload);

    public function request(string $service, string $method, array $params, int $timeout = 2) : Response;
}
