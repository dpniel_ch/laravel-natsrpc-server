<?php


namespace NatsRPC\Contract;


use NatsRPC\Server\RequestParams;

interface RouteDispatcher
{
    public function dispatch(Route $route, RequestParams $params = null);

    public function setControllerNamespace(string $namespace = null) : RouteDispatcher;
}
