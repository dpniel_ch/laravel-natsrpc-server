<?php

namespace NatsRPC\Server;

use Illuminate\Http\JsonResponse;
use NatsRPC\Contract\Request;
use NatsRPC\Contract\RouteDispatcher as RouteDispatcherContract;
use NatsRPC\Contract\MiddlewareDispatcher as MiddlewareDispatcherContract;
use NatsRPC\Contract\RequestExecutor;
use NatsRPC\Contract\RequestFactory;
use NatsRPC\Contract\RouteRegistry;
use NatsRPC\Contract\Server as ServerContract;
use NatsRPC\Exceptions\InternalErrorException;
use NatsRPC\Exceptions\JsonRpcException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RequestContext;

class Server implements ServerContract, RequestExecutor
{
    private $payload = null;

    private $requestFactory;

    /**
     * @var RouteRegistry
     */
    private $router;

    /**
     * @var RouteDispatcherContract
     */
    private $routeDispatcher;

    /**
     * @var MiddlewareDispatcherContract
     */
    private $middlewareDispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var callable[]
     */
    private $exceptionHandlers = [];

    private $middlewareContext = null;

    public function __construct(
        RequestFactory $reqFactory,
        RouteRegistry $router,
        RouteDispatcherContract $routeDispatcher,
        MiddlewareDispatcherContract $middlewareDispatcher,
        LoggerInterface $logger
    ) {
        $this->requestFactory = $reqFactory;
        $this->router = $router;
        $this->routeDispatcher = $routeDispatcher;
        $this->middlewareDispatcher = $middlewareDispatcher;
        $this->logger = $logger;
    }

    public function execute(Request $request)
    {
        try {
            $route = $this->router->resolve($request->getMethod());

            $result = $this->middlewareDispatcher->dispatch(
                $route->getMiddlewaresCollection(),
                $this->middlewareContext,
                function () use ($route, $request) {
                    return $this->routeDispatcher->dispatch($route, $request->getParams());
                }
            );
            return $request->hasId() ? new RequestResponse($request->getId(), $result) : null;
        } catch (JsonRpcException $e) {
            return $request->hasId() ? RequestResponse::constructExceptionErrorResponse($request->getId(), $e) : null;
        } catch (\Throwable $e) {
            return $this->handleException($e, $request);
        }
    }

    public function router(): RouteRegistry
    {
        return $this->router;
    }

    public function onException(string $exceptionClass, callable $handler, bool $first = false): ServerContract
    {
        if ($first) {
            $this->exceptionHandlers = [$exceptionClass => $handler] + $this->exceptionHandlers;
        } else {
            $this->exceptionHandlers[$exceptionClass] = $handler;
        }

        return $this;
    }

    public function setPayload(string $payload): void
    {
        $this->payload = $payload;
    }

    public function setControllerNamespace(string $namespace = null): ServerContract
    {
        $this->routeDispatcher->setControllerNamespace($namespace);

        return $this;
    }

    public function run($context = null, string $payload = null): JsonResponse
    {
        $this->middlewareContext = $context;

        if (null === $payload) {
            $payload = $this->payload !== null ? $this->payload : file_get_contents('php://input');
        }

        try {
            $response = $this->requestFactory->createFromPayload($payload)->executeWith($this);
        } catch (JsonRpcException $e) {
            $response = RequestResponse::constructExceptionErrorResponse(null, $e);
        } catch (\Throwable $e) {
            $response = $this->handleException($e);
        }

        return new JsonResponse($response);
    }

    public function runWithPayload(string $payload, $context = null): JsonResponse
    {
        return $this->run($context, $payload);
    }

    private function handleException(\Throwable $e, Request $request = null)
    {
        $handlerResult = $this->runExceptionHandlers($e, $request);

        if (!$handlerResult) {
            $this->logger->error($e);
        }

        if ($request && !$request->hasId()) {
            return null;
        }

        if ($handlerResult instanceof RequestResponse) {
            return $handlerResult;
        }

        return RequestResponse::constructExceptionErrorResponse(
            $request ? $request->getId() : null,
            new InternalErrorException()
        );
    }

    private function runExceptionHandlers(\Throwable $e, Request $request = null)
    {
        foreach ($this->exceptionHandlers as $className => $handler) {
            if ($e instanceof $className) {
                return $handler($e, $request);
            }
        }

        return false;
    }
}
