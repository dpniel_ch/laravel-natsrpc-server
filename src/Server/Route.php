<?php


namespace NatsRPC\Server;

use NatsRPC\Contract\MiddlewaresConfiguration;
use NatsRPC\Contract\Route as RouteContract;


final class Route implements RouteContract
{
    private $controllerClass;
    private $actionName;
    private $middlewaresCollection;
    /**
     * @param string $controllerClass
     * @param string $actionName
     * @param MiddlewaresConfiguration $middlewaresCollection
     */
    public function __construct(
        string $controllerClass,
        string $actionName,
        MiddlewaresConfiguration $middlewaresCollection
    ) {
        $this->controllerClass = $controllerClass;
        $this->actionName = $actionName;
        $this->middlewaresCollection = $middlewaresCollection;
    }

    /**
     * @return string
     */
    public function getControllerClass(): string
    {
        return $this->controllerClass;
    }

    /**
     * @return string
     */
    public function getActionName(): string
    {
        return $this->actionName;
    }

    /**
     * @return MiddlewaresConfiguration
     */
    public function getMiddlewaresCollection(): MiddlewaresConfiguration
    {
        return $this->middlewaresCollection;
    }
}
