<?php


namespace NatsRPC\Server;


use Illuminate\Contracts\Container\Container;
use NatsRPC\Contract\MiddlewareDispatcher;
use NatsRPC\Contract\RequestFactory as RequestFactoryContract;
use NatsRPC\Contract\Server as ServerContract;
use Psr\Log\LoggerInterface;

final class ServerFactory
{

    /**
     * @var Container
     */
    private $container;

    /**
     * @var RequestFactory|null
     */
    private $requestFactory = null;

    /**
     * @var MiddlewareDispatcher|null
     */
    private $middlewareDispatcher = null;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function make(): ServerContract
    {
        return new Server(
            $this->getRequestFactory(),
            new Router(),
            new RouteDispatcher($this->container),
            $this->getMiddlewareDispatcher(),
            $this->container->make(LoggerInterface::class)
        );
    }

    private function getRequestFactory(): RequestFactoryContract
    {
        if (null === $this->requestFactory) {
            $this->requestFactory = new RequestFactory();
        }

        return $this->requestFactory;
    }

    private function getMiddlewareDispatcher(): MiddlewareDispatcher
    {
        if (null === $this->middlewareDispatcher) {
            $this->middlewareDispatcher = new MiddlewarePipelineDispatcher($this->container);
        }

        return $this->middlewareDispatcher;
    }

}
