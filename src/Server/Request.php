<?php


namespace NatsRPC\Server;

use Illuminate\Contracts\Support\Jsonable;
use NatsRPC\Contract\Executable;
use NatsRPC\Contract\Request as RequestContract;
use NatsRPC\Contract\RequestExecutor;

class Request implements RequestContract, Executable
{

    /**
     * @var string
     */
    private $method;

    /**
     * @var RequestParams|null
     */
    private $params;

    /**
     * @var null|string|int
     */
    private $id = null;

    /**
     * @param string $method
     * @param RequestParams|null $params
     * @param string|int|null $id
     */
    public function __construct(string $method, RequestParams $params = null, $id = null)
    {
        $this->method = (string)$method;
        $this->params = $params ?: RequestParams::createEmpty();
        if ((null !== $id) && !is_int($id)) {
            $id = (string)$id;
        }
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return RequestParams
     */
    public function getParams(): RequestParams
    {
        return $this->params;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return $this->id !== null;
    }

    /**
     * @return null|string|int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param RequestExecutor $executor
     * @return Jsonable
     */
    public function executeWith(RequestExecutor $executor)
    {
        return $executor->execute($this);
    }

}
