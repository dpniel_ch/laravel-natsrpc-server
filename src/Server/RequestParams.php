<?php

namespace NatsRPC\Server;

use InvalidArgumentException;

final class RequestParams
{
    private $params;

    private $areParamsNamed;

    private function __construct(array $params, bool $areParamsNamed = false)
    {
        $this->params = collect($params);
        $this->areParamsNamed = $areParamsNamed;
    }

    public static function createNamed(array $params) : RequestParams
    {
        return new self($params, true);
    }

    public static function createPositional(array $params) : RequestParams
    {
        return new self(array_values($params), false);
    }

    public static function createEmpty() : RequestParams
    {
        return new self([]);
    }

    public function all() : array
    {
        return $this->params->all();
    }

    public function has(string $key) : bool
    {
        return $this->params->has($key);
    }

    public function get(string $key, $default = null)
    {
        return $this->params->get($key, $default);
    }

    public function __get($key)
    {
        if (!$this->has($key)) {
            throw new InvalidArgumentException("Parameter does not exist: '{$key}'");
        }
        return $this->params->get($key);
    }

    public function areParamsNamed() : bool
    {
        return $this->areParamsNamed;
    }
}
