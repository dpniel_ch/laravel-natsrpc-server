<?php


namespace NatsRPC\Server;


use Illuminate\Contracts\Container\Container;
use Illuminate\Pipeline\Pipeline;
use NatsRPC\Contract\MiddlewareDispatcher;
use NatsRPC\Contract\MiddlewaresConfiguration;

final class MiddlewarePipelineDispatcher implements MiddlewareDispatcher
{

    /**
     * @var Container
     */
    private $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param MiddlewaresConfiguration $middlewaresConfiguration
     * @param mixed $context
     * @param callable $next
     * @return mixed
     */
    public function dispatch(MiddlewaresConfiguration $middlewaresConfiguration, $context, callable $next)
    {
        $pipeline = new Pipeline($this->container);

        return $pipeline->send($context)->through($middlewaresConfiguration->getMiddlewares())->then($next);
    }

}
