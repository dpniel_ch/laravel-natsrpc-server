<?php


namespace NatsRPC\Server;

use NatsRPC\Contract\Executable;
use NatsRPC\Contract\RequestFactory as RequestFactoryContract;
use NatsRPC\Contract\Request as RequestContract;
use NatsRPC\Exceptions\BadRequestException;

class RequestFactory implements RequestFactoryContract
{

    /**
     * @param string $payloadJson
     * @return Executable
     */
    public function createFromPayload(string $payloadJson): Executable
    {
        try {
            $payload = json_decode($payloadJson);
        } catch (\Exception $e) {
            throw new BadRequestException();
        }

        return $this->createSingleRequest($payload);
    }

    /**
     * @param \stdClass $requestData
     * @return RequestContract
     */
    public function createRequest(\stdClass $requestData): RequestContract
    {
        return $this->createSingleRequest($requestData);
    }

    private function createSingleRequest(\stdClass $requestData): Request
    {
        if (!isset($requestData->jsonrpc) || $requestData->jsonrpc !== "2.0") {
            throw new BadRequestException();
        }
        if (empty($requestData->method) || !is_string($requestData->method)) {
            throw new BadRequestException();
        }
        $params = null;
        if (!empty($requestData->params)) {
            if (is_array($requestData->params)) {
                $params = RequestParams::createPositional($requestData->params);
            } elseif (is_object($requestData->params)) {
                $params = RequestParams::createNamed((array)$requestData->params);
            } else {
                throw new BadRequestException();
            }
        }

        return new Request(
            $requestData->method,
            $params,
            isset($requestData->id) ? $requestData->id : null
        );
    }

}
