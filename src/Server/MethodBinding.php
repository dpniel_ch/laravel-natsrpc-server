<?php


namespace NatsRPC\Server;


class MethodBinding extends Binding
{
	private $controller;
	private $action;

    public function resolveRoute(string $method): Route
    {
        return new Route($this->controller, $this->action, $this->getMiddlewaresCollection());
    }

    protected function setBinding(string $binding): void
    {
        $tokens = array_filter(explode('@', $binding, 2));
        switch (count($tokens)) {
            case 0:
                throw new \InvalidArgumentException('Empty binding');
                break;
            case 1:
                $this->controller = $tokens[0];
                $this->action = self::DEFAULT_ACTION_NAME;
                break;
            case 2:
                $this->controller = $tokens[0];
                $this->action = $tokens[1];
                break;
        }
    }
}
