<?php


namespace NatsRPC\Server;


use Illuminate\Support\Str;
use NatsRPC\Contract\Route;
use NatsRPC\Contract\RouteRegistry;
use NatsRPC\Exceptions\RouteNotFoundException;

final class Router implements RouteRegistry
{
    private $methodBindings = [];
    private $controllerBindings = [];

    private $middlewares;

    public function __construct(MiddlewaresCollection $middlewares = null)
    {
        $this->middlewares = $middlewares ?: new MiddlewaresCollection();
    }

    public function addMiddleware(string $middleware): RouteRegistry
    {
        $this->middlewares->addMiddleware($middleware);
        return $this;
    }

    public function addMiddlewares(array $middlewares): RouteRegistry
    {
        $this->middlewares->setMiddlewares($middlewares);
        return $this;
    }

    public function bind(string $method, string $binding): RouteRegistry
    {
        $this->methodBindings[Str::lower($method)] = new MethodBinding($binding, $this->middlewares);
        return $this;
    }

    public function bindController(string $namespace, string $controller): RouteRegistry
    {
        $this->controllerBindings[Str::lower($namespace)] = new ControllerBinding($controller, $this->middlewares);
        return $this;
    }

    public function group($middlewaresConfigurator, callable $routesConfigurator): RouteRegistry
    {
        $middlewaresSubcollection = $this->middlewares ? clone $this->middlewares : null;
        if (null !== $middlewaresConfigurator) {
            if (is_callable($middlewaresConfigurator)) {
                $middlewaresSubcollection = $middlewaresConfigurator($middlewaresSubcollection)
                    ?: $middlewaresSubcollection;
            } else {
                foreach ((array)$middlewaresConfigurator as $middleware) {
                    $middlewaresSubcollection->addMiddleware($middleware);
                }
            }
        }
        $subrouter = new self($middlewaresSubcollection);
        $this->mergeBindingsFrom($routesConfigurator($subrouter) ?: $subrouter);

        return $this;
    }

    public function resolve(string $method): Route
    {
        return $this->findBinding($method)->resolveRoute($method);
    }

    private function mergeBindingsFrom(Router $router): void
    {
        $this->methodBindings = collect($router->methodBindings)->merge($this->methodBindings)->toArray();
        $this->controllerBindings = collect($router->controllerBindings)->merge($this->controllerBindings)->toArray();
    }

    private function findBinding(string $method): Binding
    {
        $method = Str::lower($method);
        if (isset($this->methodBindings[$method])) {
            return $this->methodBindings[$method];
        }

        $namespace = strtok($method, '.');
        if (isset($this->controllerBindings[$namespace])) {
            return $this->controllerBindings[$namespace];
        }

        throw new RouteNotFoundException($method);
    }
}
