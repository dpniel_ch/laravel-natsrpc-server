<?php


namespace NatsRPC\Server;


use Illuminate\Support\Collection;
use NatsRPC\Contract\MiddlewaresConfiguration;

class MiddlewaresCollection extends Collection implements MiddlewaresConfiguration
{
    public function getMiddlewares(): array
    {
        return $this->all();
    }

    public function setMiddlewares(array $middlewares = []): MiddlewaresConfiguration
    {
        $this->merge($middlewares);
        return $this;
    }

    public function addMiddleware(string $middleware): MiddlewaresConfiguration
    {
        $this->push($middleware);
        return $this;
    }
}
