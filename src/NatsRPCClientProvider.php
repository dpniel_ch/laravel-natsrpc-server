<?php

namespace NatsRPC;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use NatsRPC\Client\Client;
use NatsRPC\Contract\Client as ClientInterface;
use Spiral\Goridge\Relay;
use Spiral\Goridge\RelayInterface;
use Spiral\Goridge\RPC;
use Spiral\Goridge\SocketRelay;

class NatsRPCClientProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->singleton(RelayInterface::class, function ($app) {
            $service = config("natsrpc.service");
            $user = config("natsrpc.nats.user", "");
            $sock = "unix:///tmp/{$service}.sock";
            if ("" !== $user) {
                $sock = "unix:///tmp/{$service}.{$user}.sock";
            }
            return Relay::create($sock);
        });

        $this->app->singleton(RPC::class, function ($app) {
            return new RPC(app()->make(RelayInterface::class));
        });

        // $this->app->singleton(SocketRelay::class, function ($app, $args) {
        //     return new RPC(app()->makeWith(RelayInterface::class, $args));
        // });

        $this->app->singleton(ClientInterface::class, function ($app) {
            return new Client();
        });
    }

    public function provides()
    {
        return [Client::class, RelayInterface::class, RPC::class];
    }
}
