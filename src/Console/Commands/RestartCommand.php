<?php


namespace NatsRPC\Console\Commands;


use Symfony\Component\Process\Process;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use NatsRPC\Client\Client;

class RestartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nats:restart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restarts natsrpc server';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $client = app()->make(Client::class);
        $client->raw(sprintf("%s.reset", config("natsrpc.service")), ["noop" => ""]);
    }
}
