<?php


namespace NatsRPC\Console\Commands;


use Symfony\Component\Process\Process;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ListenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nats:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start a nats listener';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $binary = __DIR__ . "/../../../bin/natsrunner-linux";
        if (php_uname('s') === "Darwin") {
            $binary = __DIR__ . "/../../../bin/natsrunner";
        }

        Process::fromShellCommandline("chmod +x " . $binary)->run();


        $admin = config("natsrpc.server.admin", false) ? "true" : "false";

        $commandFile = base_path(Str::after(config("natsrpc.server.command"), "php "));

        if (!file_exists($commandFile)) {
            $this->error("Command file not found: {$commandFile}");
            return;
        }

        $process = new Process([
            $binary,
            "--name=" . config("natsrpc.service"),
            "--command=" . "php {$commandFile}",
            "--workers=" . config("natsrpc.server.pool.num_workers"),
            "--jobs=" . config("natsrpc.server.pool.max_jobs"),
            "--nats-host=" . config("natsrpc.nats.host"),
            "--nats-user=" . config("natsrpc.nats.user"),
            "--nats-pass=" . config("natsrpc.nats.pass"),
            "--subscribe=" . implode(",", config("natsrpc.subscribe", [])),
            "--admin={$admin}",
        ]);
        $process->setTimeout(null);
        $process->setIdleTimeout(null);

        pcntl_async_signals(true);

        $cleanup = function () use ($process) {
            if (!$process->isRunning()) {
                return;
            }
            $this->line('Shutting down...');
            $process->stop();
        };

        pcntl_signal(SIGINT, $cleanup);
        pcntl_signal(SIGTERM, $cleanup);
        pcntl_signal(SIGQUIT, $cleanup);

        $process->start();

        while ($process->isRunning()) {
            $iterator = $process->getIterator();
            foreach ($iterator as $data) {
                echo $data;
            }
        }
    }
}
