<?php

namespace NatsRPC\Client;

use NatsRPC\Contract\Response as ResponseInterface;

class Response implements ResponseInterface
{
    private $resp;

    public function __construct($response)
    {
        $this->resp = collect(json_decode($response, true));
    }

    public function id()
    {
        return $this->resp->get("id", null);
    }

    public function hasError()
    {
        return $this->resp->has("error");
    }

    public function hasResult()
    {
        return $this->resp->has("result");
    }

    public function error()
    {
        if (!$this->hasError()) {
            return null;
        }
        return $this->resp->get("error");
    }

    public function result()
    {
        if (!$this->hasResult()) {
            return null;
        }
        return $this->resp->get("result");
    }
}
