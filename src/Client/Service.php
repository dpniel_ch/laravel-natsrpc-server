<?php

namespace NatsRPC\Client;

use NatsRPC\Contract\Client as NatsClient;

class Service
{
    protected $service;
    protected $client;

    public function __construct(string $service)
    {
        $this->service = $service;
        $this->client = app()->make(NatsClient::class);
    }

    public static function create(string $service)
    {
        return new self($service);
    }

    public function request(string $method, array $data = [], int $timeout = 2) : Response
    {
        return $this->client->request($this->service, $method, $data, $timeout);
    }
}
