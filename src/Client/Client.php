<?php

namespace NatsRPC\Client;

use NatsRPC\Contract\Client as ClientInterface;
use NatsRPC\Contract\Response as ResponseInterface;
use Spiral\Goridge;
use Spiral\Goridge\RelayInterface;
use Spiral\Goridge\RPC;

class Client implements ClientInterface
{
    private $service;
    private $rpc;

    public function __construct(?string $service = null)
    {
        $this->service = config("natsrpc.service");
        // Means we can switch it out for our own relay in tests etc
        $this->rpc = app()->make(RPC::class);
    }

    public function raw(string $topic, array $payload)
    {
        return $this->rpc->call("Client.Publish", json_encode([
            "service" => $topic,
            "data" => $payload
        ]));
    }

    public function publish(string $event, array $payload)
    {
        return $this->rpc->call("Client.Publish", json_encode([
            "service" => $this->service . ".events",
            "data" => [
                "jsonrpc" => "2.0",
                "id" => null,
                "method" => $this->service . "_" . $event,
                "params" => $payload
            ]
        ]));
    }

    public function request(string $service, string $method, array $params, int $timeout = 2) : ResponseInterface
    {
        $resp = $this->rpc->call("Client.Request", json_encode([
            "service" => $service,
            "data" => [
                "jsonrpc" => "2.0",
                "id" => uniqid($this->service),
                "method" => $method,
                "params" => $params
            ],
            "timeout" => $timeout
        ]));
        return new Response($resp);
    }

}
