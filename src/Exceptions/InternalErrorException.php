<?php


namespace NatsRPC\Exceptions;

use NatsRPC\Server\ErrorCode;

final class InternalErrorException extends JsonRpcException
{

    protected function getDefaultMessage(): string
    {
        return 'Internal error';
    }

    protected function getDefaultCode(): int
    {
        return ErrorCode::INTERNAL_ERROR;
    }

}
