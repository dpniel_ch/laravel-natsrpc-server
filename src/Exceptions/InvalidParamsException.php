<?php


namespace NatsRPC\Exceptions;


use NatsRPC\Server\ErrorCode;

final class InvalidParamsException extends JsonRpcException
{

    protected function getDefaultMessage(): string
    {
        return 'Invalid params';
    }

    protected function getDefaultCode(): int
    {
        return ErrorCode::INVALID_PARAMS;
    }

}
