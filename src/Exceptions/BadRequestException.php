<?php


namespace NatsRPC\Exceptions;

use NatsRPC\Server\ErrorCode;

final class BadRequestException extends JsonRpcException
{

    protected function getDefaultMessage(): string
    {
        return 'Invalid Request';
    }

    protected function getDefaultCode(): int
    {
        return ErrorCode::INVALID_REQUEST;
    }

}
