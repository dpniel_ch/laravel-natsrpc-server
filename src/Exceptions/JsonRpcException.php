<?php


namespace NatsRPC\Exceptions;


abstract class JsonRpcException extends \RuntimeException
{
    public function __construct(string $message = "", int $code = 0, \Exception $previous = null)
    {
        parent::__construct(
            $message ?: $this->getDefaultMessage(),
            (int)($code ?: $this->getDefaultCode()),
            $previous
        );
    }

    abstract protected function getDefaultMessage(): string;

    abstract protected function getDefaultCode(): int;

    public function getExtras(): array {
        return [];
    }

}
