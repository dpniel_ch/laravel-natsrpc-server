<?php


namespace NatsRPC\Exceptions;


use NatsRPC\Server\ErrorCode;

final class RouteNotFoundException extends JsonRpcException
{

    protected function getDefaultMessage(): string
    {
        return "Method not found";
    }

    protected function getDefaultCode(): int
    {
        return ErrorCode::METHOD_NOT_FOUND;
    }
}
