<?php


namespace NatsRPC\Listeners;

use Illuminate\Support\Facades\Log;
use NatsRPC\Contract\EventHandler;
use NatsRPC\Server\RequestParams;

class LogEventHandler implements EventHandler
{
    public function handle(RequestParams $params)
    {
        Log::info("Received event: " . print_r($params->all(), true));
    }
}
