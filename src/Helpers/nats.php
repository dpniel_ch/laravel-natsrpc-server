<?php

use NatsRPC\Client\Client;

if (! function_exists('nats')) {
    /**
     * Dispatch an event and call the listeners.
     *
     * @param  string  $event
     * @param  array  $payload
     */
    function nats(string $event, array $payload)
    {
        return app()->make(Client::class)->publish($event, $payload);
    }
}
