<?php

namespace NatsRPC;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use NatsRPC\Console\Commands\KillCommand;
use NatsRPC\Console\Commands\ListenCommand;
use NatsRPC\Console\Commands\MakeNatsController;
use NatsRPC\Console\Commands\PauseCommand;
use NatsRPC\Console\Commands\RestartCommand;
use NatsRPC\Contract\Server;
use NatsRPC\Events\AdminServiceEvent;
use NatsRPC\Events\ServiceEvent;
use NatsRPC\Server\RequestFactory;
use NatsRPC\Server\ServerFactory;

class NatsRPCServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('natsrpc.php'),
            ], 'config');

            $this->commands([
                ListenCommand::class,
                KillCommand::class,
                RestartCommand::class,
                PauseCommand::class,
                MakeNatsController::class
            ]);
        }

        if ($this->app->routesAreCached()) {
            $this->loadCachedRoutes();
        } else {
            $this->loadRoutes();
        }
    }

    /**
     * Load the cached routes for the application.
     *
     * @return void
     */
    protected function loadCachedRoutes()
    {
        $this->app->booted(function () {
            require $this->app->getCachedRoutesPath();
        });
    }

    /**
     * Load the application routes.
     *
     * @param Router $router
     * @return void
     */
    protected function loadRoutes(): void
    {
        $this->app->router->group(
            ['namespace' => 'App\Nats\Controllers', 'prefix' => 'nats'],
            function ($router) {
                $rpcServer = $this->app->make(Server::class);
                $rpcServer->setControllerNamespace("App\Nats\Controllers");
                $rpcRouter = $rpcServer->router();
                $router->post("/rpc", function (Request $request) use ($rpcServer, $rpcRouter) {
                    require base_path('routes/nats.php');
                    $rpcServer->setPayload($request->getContent());
                    return $rpcServer->run();
                });

                $router->post("/event", function (Request $request) {
                    $factory = new RequestFactory();
                    $req = $factory->createFromPayload($request->getContent());
                    $parts = explode("_", $req->getMethod(), 2);
                    event(new ServiceEvent($parts[0], $parts[1], $req->getParams()));
                    return [];
                });

                $router->post("/admin", function (Request $request) {
                    $decoded = json_decode($request->getContent(), true);
                    Log::info("Decoded admin: " . print_r($decoded, true));
                    if (isset($decoded["Type"])) {
                        event(new AdminServiceEvent($decoded["Name"], $decoded["Type"], $decoded));
                    } else {
                        event(new AdminServiceEvent($decoded["Name"], "service.info", $decoded));
                    }
                    return [];
                });

            }
        );
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'natsrpc');

        $this->app->register(NatsRPCClientProvider::class);

        $this->app->singleton(
            ServerFactory::class,
            function () {
                return new ServerFactory($this->app);
            }
        );

        $this->app->singleton(
            Server::class,
            function () {
                return $this->app->make(ServerFactory::class)->make();
            }
        );
    }
}
