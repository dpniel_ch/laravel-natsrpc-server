<?php

namespace NatsRPC\Controllers;

use NatsRPC\Traits\NatsRpcValidationMethods;

class NatsRpcController
{
    use NatsRpcValidationMethods;
}
