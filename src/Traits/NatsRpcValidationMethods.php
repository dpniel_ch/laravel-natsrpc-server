<?php

namespace NatsRPC\Traits;

use Illuminate\Support\Str;
use NatsRPC\Exceptions\InvalidParamsException;
use NatsRPC\Server\RequestParams;

trait NatsRpcValidationMethods
{
    public function validate(RequestParams $params, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($params->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            throw new InvalidParamsException($validator->errors()[0]);
        }

        return collect($params->all())->only(collect($rules)->keys()->map(function ($rule) {
            return Str::contains($rule, ".") ? explode(".", $rule)[0] : $rule;
        })->unique()->toArray());
    }

    private function getValidationFactory()
    {
        return app('validator');
    }
}
