<?php


namespace NatsRPC\Tests;

use DateTime;
use Mockery;
use NatsRPC\Client\Response;
use NatsRPC\Client\Service;
use NatsRPC\Contract\Client as NatsClient;
use Spiral\Goridge\Relay;
use Spiral\Goridge\RelayInterface;
use Spiral\Goridge\RPC;
use Spiral\Goridge\SocketRelay;

class ProviderTest extends TestCase
{
    /** @test */
    public function test_client_instance()
    {
        $mock = Mockery::mock(NatsClient::class);

        $this->app->instance(NatsClient::class, $mock);

        $client = app()->make(NatsClient::class);

        self::assertSame($mock, $client);
    }

    /** @test */
    public function test_service_instance()
    {
        $mock = Mockery::mock(NatsClient::class);

        $mock->shouldReceive("request")->andReturn(
            new Response(json_encode([]))
        );

        $this->app->instance(NatsClient::class, $mock);

        $service = new Service("mock-service");

        $resp = $service->request("something", ["hello" => "world"]);

        self::assertInstanceOf(Response::class, $resp);

        // $client->shouldReceive("request")->andReturn(
        //     new Response(json_encode([]))
        // );

        // dd($client->request("test", "something", ["hello" => "world", 60]));
    }

    /** @test */
    public function test_rpc_instance()
    {
        $rpc = app()->makeWith(RPC::class, ["socket" => "unix:///tmp/test.sock"]);
        self::assertInstanceOf(RPC::class, $rpc);
    }

    /** @test */
    public function test_relay_instance()
    {
        $relay = app()->makeWith(RelayInterface::class, ["socket" => "unix:///tmp/test.sock"]);
        self::assertInstanceOf(RelayInterface::class, $relay);
        self::assertInstanceOf(SocketRelay::class, $relay);
    }
}
