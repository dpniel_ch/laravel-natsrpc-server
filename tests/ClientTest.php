<?php


namespace NatsRPC\Tests;

use DateTime;
use Mockery;
use NatsRPC\Client\Response;
use NatsRPC\Client\Service;
use NatsRPC\Contract\Client as NatsClient;
use NatsRPC\Contract\Response as ContractResponse;
use Spiral\Goridge\RelayInterface;
use Spiral\Goridge\RPC;

class ClientTest extends TestCase
{
    /** @test */
    public function test_client_raw()
    {
        $mock = Mockery::mock(RPC::class);

        $payload = ["raw" => "data"];

        $mock->shouldReceive("call")
            ->once()
            ->with(
                "Client.Publish",
                json_encode([
                    "service" => "com.service.test",
                    "data" => $payload
                ])
            )->andReturn(true);

        $this->app->instance(RPC::class, $mock);
        $client = app()->make(NatsClient::class);

        $resp = $client->raw("com.service.test", $payload);
        self::assertTrue($resp);
    }

    /** @test */
    public function test_client_publish()
    {
        $mock = Mockery::mock(RPC::class);

        $payload = ["raw" => "data"];

        $mock->shouldReceive("call")
            ->once()
            ->with(
                "Client.Publish",
                json_encode([
                    "service" => "com.service.test.events",
                    "data" => [
                        "jsonrpc" => "2.0",
                        "id" => null,
                        "method" => "com.service.test_phpunitevent",
                        "params" => $payload
                    ]
                ])
            )->andReturn(true);

        $this->app->instance(RPC::class, $mock);
        $client = app()->make(NatsClient::class);

        $resp = $client->publish("phpunitevent", $payload);
        self::assertTrue($resp);
    }

    /** @test */
    public function test_client_request()
    {
        $mock = Mockery::mock(RPC::class);

        $payload = ["raw" => "data"];

        $mock->shouldReceive("call")
            ->once()->andReturn(json_encode($payload));

        $this->app->instance(RPC::class, $mock);
        $client = app()->make(NatsClient::class);

        $resp = $client->request("com.service.test", "foo", $payload);
        self::assertInstanceOf(ContractResponse::class, $resp);
    }
}
