<?php


namespace NatsRPC\Tests;

use Illuminate\Foundation\Application;
use Mockery;
use Orchestra\Testbench\TestCase as Orchestra;
use NatsRPC\Contract\Client as ClientInterface;
use NatsRPC\NatsRPCServiceProvider;

abstract class TestCase extends Orchestra
{

    public function setUp() : void
    {
        parent::setUp();
    }

    /**
     * @param Application $app
     *
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            NatsRPCServiceProvider::class,
        ];
    }
}
