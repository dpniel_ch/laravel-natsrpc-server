package cmd

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/dpniel_ch/laravel-natsrpc-server/service"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var srv *service.Server

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "natsrunner",
	Short: "Laravel NatsRPC Runner",
	Run: func(cmd *cobra.Command, args []string) {

		// Go signal notification works by sending `os.Signal`
		// values on a channel. We'll create a channel to
		// receive these notifications (we'll also make one to
		// notify us when the program can exit).
		sigs := make(chan os.Signal, 1)
		done := make(chan bool, 1)

		// `signal.Notify` registers the given channel to
		// receive notifications of the specified signals.
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

		// This goroutine executes a blocking receive for
		// signals. When it gets one it'll print it out
		// and then notify the program that it can finish.
		go func() {
			sig := <-sigs
			fmt.Println()
			fmt.Println(sig)
			done <- true
		}()

		// Start NATS-RPC Server
		srv = service.NewServer(&service.ServerConfig{
			Name:       name,
			Command:    command,
			NumWorkers: int64(numWorkers),
			MaxJobs:    int64(maxJobs),
			Subscribe:  subscribe,
			Host:       natsHost,
			User:       natsUser,
			Pass:       natsPass,
			Admin:      admin,
		})
		defer srv.Stop()
		srv.Start()

		// Start NATS-RPC Client Bridge
		client := service.NewRpcClient(&service.ClientConfig{
			Name: name,
			Host: natsHost,
			User: natsUser,
			Pass: natsPass,
		})

		go client.Listen()

		log.Info("Serving.....")

		<-done
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().BoolVarP(&admin, "admin", "", false, "Run as a admin service. Enables service event listeners")
	rootCmd.Flags().StringVarP(&name, "name", "n", "com.service.example", "Service name to register as with nats")
	rootCmd.Flags().StringVarP(&command, "command", "c", "php vendor/bin/natsrunner.php", "Worker command")
	rootCmd.Flags().IntVarP(&numWorkers, "workers", "w", 5, "Number of workers")
	rootCmd.Flags().IntVarP(&maxJobs, "jobs", "j", 5, "Max number of jobs per worker")
	rootCmd.Flags().StringVarP(&natsHost, "nats-host", "H", "nats://127.0.0.1", "Nats host connection string")
	rootCmd.Flags().StringVarP(&natsUser, "nats-user", "U", "", "Nats user")
	rootCmd.Flags().StringVarP(&natsPass, "nats-pass", "P", "", "Nats password")
	rootCmd.Flags().StringVarP(&subscribe, "subscribe", "s", "", "Service subscriptions comma seperated")
	// rootCmd.PersistentFlags().BoolVarP(&flagDebug, "debug", "d", false, "Show command output without execution")
	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.yoda.yaml)")
}
