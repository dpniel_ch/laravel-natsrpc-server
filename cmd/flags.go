package cmd

var (
	admin      bool
	name       string
	command    string
	numWorkers int
	maxJobs    int
	natsHost   string
	natsUser   string
	natsPass   string
	subscribe  string
)
