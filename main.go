package main

import (
	"bitbucket.org/dpniel_ch/laravel-natsrpc-server/cmd"
)

func main() {
	cmd.Execute()
}
