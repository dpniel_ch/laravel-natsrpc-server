<?php

return [
    "service" => env("NATSRPC_SERVICE_NAME", "com.service.test"),
    "server" => [
        "admin" => false,
        "command" => env("NATSRPC_WORKER_CMD", "php vendor/bin/natsrunner.php"),
        "pool" => [
            "num_workers" => env("NATSRPC_NUM_WORKERS", 5),
            "max_jobs" => env("NATSRPC_MAX_JOBS", 100)
        ]
    ],
    "nats" => [
        "host" => env("NATS_HOST", "nats://127.0.0.1"),
        "user" => env("NATS_USER", ""),
        "pass" => env("NATS_PASS", "")
    ],
    // String list of service names to event subscribe to.
    "subscribe" => [

    ]
];
