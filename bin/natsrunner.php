<?php

use Illuminate\Http\JsonResponse;
use Spiral\Goridge;
use Spiral\RoadRunner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use NatsRPC\Exceptions\JsonException;

ini_set('display_errors', 'stderr');
// TODO: Tidy this up it is Ugly AF!!
$bootstrap = __DIR__ . "/../../../../bootstrap/app.php";
if (file_exists(__DIR__ . '/../vendor_php/autoload.php')) {
    require __DIR__ . '/../vendor_php/autoload.php';
    $bootstrap = __DIR__ . "/../bootstrap/app.php";
} else {
    require __DIR__ . '/../../../autoload.php';
}

$isLaravel = true;
$_app = null;
$_kernel = null;
if (file_exists($bootstrap)) {
    $_app = require_once $bootstrap;
    if (substr($_app->version(), 0, 5) === 'Lumen') {
        $isLaravel = false;
    }
}

if (!is_null($_app)) {
    $_kernel = $_app->make($isLaravel ? 'Illuminate\Contracts\Http\Kernel' : 'Laravel\Lumen\Application');
}

$rr = new RoadRunner\Worker(new Goridge\StreamRelay(STDIN, STDOUT));

try {
    while ($body = $rr->receive($context)) {
        $decoded = json_decode($body, true);
        if (is_null($decoded)) {
            throw new JsonException("Invalid json data");
        }
        $route = "";
        if (array_key_exists("id", $decoded)) {
            $route = is_null($decoded["id"]) ? "/nats/event" : "/nats/rpc";
        } else {
            $route = "/nats/admin";
        }
        $request = Request::createFromBase(
            SymfonyRequest::create(
                $route, 'POST', $decoded,
                [], [], [
                    "HTTP_USER_AGENT" => "NatsRPC",
                    "HTTP_ACCEPT" => "application/json",
                    "CONTENT_TYPE" => "application/json"
                ], $body)
        );

        if (is_null($_app)) {
            $response = new JsonResponse($request->all());
            $rr->send($response->getContent());
        } else {
            $response = $_kernel->handle($request);
            $rr->send($response->getContent() ?? "");
        }
    }
} catch (\Throwable $e) {
    $rr->error((string)$e);
}
