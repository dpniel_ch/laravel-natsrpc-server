<?php

use Illuminate\Http\JsonResponse;
use Spiral\Goridge;
use Spiral\RoadRunner;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use NatsRPC\Exceptions\JsonException;

ini_set('display_errors', 'stderr');
if (file_exists('vendor_php/autoload.php')) {
    require 'vendor_php/autoload.php';
} else {
    require 'vendor/autoload.php';
}


$rr = new RoadRunner\Worker(new Goridge\StreamRelay(STDIN, STDOUT));

try {
    while ($body = $rr->receive($context)) {
        $decoded = json_decode($body, true);
        if (is_null($decoded)) {
            throw new JsonException("Invalid json data");
        }
        $request = Request::createFromBase(
            SymfonyRequest::create("/nats/rpc", 'POST', $decoded, [], [], [], $body)
        );
        $response = new JsonResponse($request->all());
        $rr->send($response->getContent());
    }
} catch (\Throwable $e) {
    $rr->error((string)$e);
}
