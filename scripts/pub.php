<?php
use Spiral\Goridge;
require "vendor_php/autoload.php";

// $rpc = new Goridge\RPC(new Goridge\SocketRelay("127.0.0.1", 6001));
$rpc = new Goridge\RPC(Goridge\Relay::create('unix:///tmp/nats.sock'));
$i = 0;

$end = Carbon\Carbon::now()->addSecond();
while (Carbon\Carbon::now() < $end) {
    $rpc->call("Client.Publish", json_encode(["service" => "other", "data" => ["test" => "data"]]));
    $i++;
}

echo "ReqPSec: " . $i . PHP_EOL;
