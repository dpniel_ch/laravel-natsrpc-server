<?php

/**
 * You can bind a controller like this
 *
 * All functions can be access with dot-notation in the json-rpc method field
 *
 * {method: "foo.index"}
 *
 * Would call the index function on the FooController
 */
$rpcRouter->bindController('foo', 'FooController');
/**
 * Or you can bind single functions like so
 *
 * {method: "bar"}
 *
 * Would call bar function on FooController
 */
$rpcRouter->bind('bar', 'FooController@bar');
